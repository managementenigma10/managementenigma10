﻿using System;
using Enigma10.Enitities;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma10.Database
{
    class DataContext : DbContext, IDisposable
    {
        public DataContext() : base("EnigmaDatabase")
        {

        }
        public int news;
        public DbSet<Employee> employees { get; set; }
        public DbSet<Content> contents { get; set; }
        public DbSet<Category> categories { get; set; }
        public DbSet<SubCategory> subCategories { get; set; }
        public DbSet<PlayerManagement> playerManagements { get; set; }
        public DbSet<Rank> ranks { get; set; }
        public DbSet<Title> titles { get; set; }

    }
}
