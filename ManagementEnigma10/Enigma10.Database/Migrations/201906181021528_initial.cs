namespace Enigma10.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        CategoryId = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        Content_ContentId = c.Int(),
                    })
                .PrimaryKey(t => t.CategoryId)
                .ForeignKey("dbo.Contents", t => t.Content_ContentId)
                .Index(t => t.Content_ContentId);
            
            CreateTable(
                "dbo.SubCategories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        SubCatogeryName = c.String(),
                        categoryFK_CategoryId = c.Int(),
                        Content_ContentId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.categoryFK_CategoryId)
                .ForeignKey("dbo.Contents", t => t.Content_ContentId)
                .Index(t => t.categoryFK_CategoryId)
                .Index(t => t.Content_ContentId);
            
            CreateTable(
                "dbo.Contents",
                c => new
                    {
                        ContentId = c.Int(nullable: false, identity: true),
                        Question = c.String(),
                        OptionA = c.String(),
                        OptionB = c.String(),
                        OptionC = c.String(),
                        OptionD = c.String(),
                        A = c.Boolean(nullable: false),
                        B = c.Boolean(nullable: false),
                        C = c.Boolean(nullable: false),
                        D = c.Boolean(nullable: false),
                        Events_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ContentId)
                .ForeignKey("dbo.Events", t => t.Events_ID)
                .Index(t => t.Events_ID);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        EmployeeId = c.Int(nullable: false, identity: true),
                        EmployeeName = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        CNIC = c.String(),
                        Contact = c.String(),
                        EmailAddress = c.String(),
                        Country = c.String(),
                        AlternateEmailAddress = c.String(),
                        City = c.String(),
                        ResidentialAddress = c.String(),
                        BankAccountNumber = c.String(),
                        Designation = c.String(),
                        DOB = c.String(),
                        JoinDate = c.String(),
                        Gender = c.String(),
                        Roles = c.String(),
                        Content_ContentId = c.Int(),
                    })
                .PrimaryKey(t => t.EmployeeId)
                .ForeignKey("dbo.Contents", t => t.Content_ContentId)
                .Index(t => t.Content_ContentId);
            
            CreateTable(
                "dbo.PlayerManagements",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PlayerName = c.String(),
                        Username = c.String(),
                        Password = c.String(),
                        CNIC = c.String(),
                        Contact = c.String(),
                        Qualification = c.String(),
                        EmailAddress = c.String(),
                        Country = c.String(),
                        AlternateEmailAddress = c.String(),
                        City = c.String(),
                        ResidentialAddress = c.String(),
                        BankAccountNumber = c.String(),
                        CoinsWon = c.Int(nullable: false),
                        CashWon = c.Double(nullable: false),
                        Designation = c.String(),
                        DOB = c.String(),
                        JoinDate = c.String(),
                        Gender = c.String(),
                        EventPK_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Events", t => t.EventPK_ID)
                .Index(t => t.EventPK_ID);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        EventName = c.String(),
                        EventPrice = c.Double(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        EndTime = c.DateTime(nullable: false),
                        NumberOfPlayers = c.Int(nullable: false),
                        CoinsRequired = c.Int(nullable: false),
                        NumberOfQuestions = c.Int(nullable: false),
                        NumberOfLevels = c.Int(nullable: false),
                        City = c.String(),
                        EventActive = c.String(),
                        Country = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Ranks",
                c => new
                    {
                        RankId = c.Int(nullable: false, identity: true),
                        RankPosition = c.String(),
                        playerPK_ID = c.Int(),
                    })
                .PrimaryKey(t => t.RankId)
                .ForeignKey("dbo.PlayerManagements", t => t.playerPK_ID)
                .Index(t => t.playerPK_ID);
            
            CreateTable(
                "dbo.Titles",
                c => new
                    {
                        TitleId = c.Int(nullable: false, identity: true),
                        TitleName = c.String(),
                        playerPK_ID = c.Int(),
                    })
                .PrimaryKey(t => t.TitleId)
                .ForeignKey("dbo.PlayerManagements", t => t.playerPK_ID)
                .Index(t => t.playerPK_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Titles", "playerPK_ID", "dbo.PlayerManagements");
            DropForeignKey("dbo.Ranks", "playerPK_ID", "dbo.PlayerManagements");
            DropForeignKey("dbo.PlayerManagements", "EventPK_ID", "dbo.Events");
            DropForeignKey("dbo.Contents", "Events_ID", "dbo.Events");
            DropForeignKey("dbo.SubCategories", "Content_ContentId", "dbo.Contents");
            DropForeignKey("dbo.Employees", "Content_ContentId", "dbo.Contents");
            DropForeignKey("dbo.Categories", "Content_ContentId", "dbo.Contents");
            DropForeignKey("dbo.SubCategories", "categoryFK_CategoryId", "dbo.Categories");
            DropIndex("dbo.Titles", new[] { "playerPK_ID" });
            DropIndex("dbo.Ranks", new[] { "playerPK_ID" });
            DropIndex("dbo.PlayerManagements", new[] { "EventPK_ID" });
            DropIndex("dbo.Employees", new[] { "Content_ContentId" });
            DropIndex("dbo.Contents", new[] { "Events_ID" });
            DropIndex("dbo.SubCategories", new[] { "Content_ContentId" });
            DropIndex("dbo.SubCategories", new[] { "categoryFK_CategoryId" });
            DropIndex("dbo.Categories", new[] { "Content_ContentId" });
            DropTable("dbo.Titles");
            DropTable("dbo.Ranks");
            DropTable("dbo.Events");
            DropTable("dbo.PlayerManagements");
            DropTable("dbo.Employees");
            DropTable("dbo.Contents");
            DropTable("dbo.SubCategories");
            DropTable("dbo.Categories");
        }
    }
}
