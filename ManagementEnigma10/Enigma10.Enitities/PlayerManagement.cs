﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma10.Enitities
{
    public class PlayerManagement
    {

        public int ID { get; set; }
        public string PlayerName { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string CNIC { get; set; }
        public string Contact { get; set; }
        public string Qualification { get; set; }
        public string EmailAddress { get; set; }
        public string Country { get; set; }
        public string AlternateEmailAddress { get; set; }
        public string City { get; set; }
        public string ResidentialAddress { get; set; }

        public string BankAccountNumber { get; set; }
        public int CoinsWon { get; set; }
        public double CashWon { get; set; }
        public string Designation { get; set; }
        public string DOB { get; set; }
        public string JoinDate { get; set; }
        public string Gender { get; set; }

        public Events EventPK { get; set; }

    }
}
