﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma10.Enitities
{
    public class Content
    {

        public int ContentId { get; set; }
        public string Question { get; set; }
        public string OptionA { get; set; }
        public string OptionB { get; set; }
        public string OptionC { get; set; }
        public string OptionD { get; set; }
        public bool A { get; set; }
        public bool B { get; set; }
        public bool C { get; set; }
        public bool D { get; set; }

        public ICollection<Employee> EmployeeFK { get; set; }
        public ICollection<Category> CategoryFK { get; set; }
        public ICollection<SubCategory> SubCategoriesFK { get; set; }

    }
}
