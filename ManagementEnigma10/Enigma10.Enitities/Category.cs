﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma10.Enitities
{
    public class Category
    {

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public ICollection<SubCategory> SubCategoryFK { get; set; }

    }
}
