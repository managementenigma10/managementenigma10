﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enigma10.Enitities
{
    public class Events
    {
        public int ID { get; set; }
        public string EventName { get; set; }
        public double EventPrice { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int NumberOfPlayers { get; set; }
        public int CoinsRequired { get; set; }
        public int NumberOfQuestions { get; set; }
        public int NumberOfLevels { get; set; }
        public string City { get; set; }
        public string EventActive { get; set; }
        public string Country { get; set; }

        public ICollection<Content> contentFk { get; set; }
        public ICollection<PlayerManagement> playerFk { get; set; }

    }
}
